<p style="text-align: center">
 <img src="https://img.shields.io/badge/spring%20cloud%20base-2.3.2-success.svg" alt="Build Status">
 <img src="https://img.shields.io/badge/Spring%20Cloud-Hoxto.SR8-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Spring%20Boot-2.3.2.RELEASE-green.svg" alt="Downloads">
 <img src="https://img.shields.io/badge/Spring%20Cloud%20Alibaba-2.2.5.RELEASE-orange.svg" alt="Downloads">
 <img src="https://img.shields.io/github/license/pig-mesh/pig" alt="license"/>
</p>

## wenming5112
git token: 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
之后用自己生成的token登录，把上面生成的token粘贴到输入密码的位置，然后成功push代码

## 系统说明

- 基于 Spring Cloud Hoxton 、Spring Boot 2.3、 OAuth2 的 RBAC **权限管理系统**
- 基于数据驱动视图的理念封装 element-ui，即使没有 vue 的使用经验也能快速上手
- 提供对常见容器化支持 Docker、Kubernetes、Rancher2 支持
- 提供 lambda 、stream api 、webflux 的生产实践

### 核心依赖

| 依赖                   | 版本          |
| ---------------------- | ------------- |
| Spring Boot            | 2.3.2.RELEASE |
| Spring Cloud           | Hoxton.SR8    |
| Spring Cloud Alibaba   | 2.2.5.RELEASE |
| Spring Security OAuth2 | 2.3.2         |
| Mybatis Plus           | 3.4.0         |
| Druid                  | 1.2.5         |
| hutool                 | 5.4.7         |
| Avue                   | 2.6.16        |

### 模块说明

```text
# 前端(后台管理)项目地址
pig-ui  -- https://gitee.com/log4j/pig-ui
# 后端模块结构
spring-cloud-base
.
|-- cloud-base-auth -- 授权服务提供 [3000]
|-- cloud-base-biz -- 通用用户权限管理模块
|   |-- cloud-base-sso-demo-system -- SSO单点登录测试应用1 [4011]
|   |-- cloud-base-sso-demo-test -- SSO单点登录测试应用2 [4012]
|   |-- cloud-base-upms-api -- 通用用户权限管理系统公共api模块
|   `-- cloud-base-upms-biz -- 通用用户权限管理系统业务处理模块 [4000]
|-- cloud-base-common -- 系统公共模块
|   |-- cloud-base-common-core -- 公共工具类核心包
|   |-- cloud-base-common-desensitization -- 脱敏
|   |-- cloud-base-common-druid-datasource -- druid数据源包
|   |-- cloud-base-common-dynamic-datasource -- 动态数据源包
|   |-- cloud-base-common-express -- 通用快递查询
|   |-- cloud-base-common-feign -- feign依赖
|   |-- cloud-base-common-idempotent -- 幂等
|   |-- cloud-base-common-job -- xxl-job 封装
|   |-- cloud-base-common-log -- 日志服务
|   |-- cloud-base-common-message -- 消息模块(短信、邮件验证码，或者模板消息)
|   |-- cloud-base-common-model -- 通用实体
|   |-- cloud-base-common-oss -- 对象存储
|   |-- cloud-base-common-rabbitmq -- 消息队列
|   |-- cloud-base-common-redis -- Redis缓存自动配置模块
|   |-- cloud-base-common-security -- 安全工具类
|   |-- cloud-base-common-sentinel -- sentinel 扩展封装
|   |-- cloud-base-common-swagger -- 接口文档
|   `-- cloud-base-common-test -- oauth2.0 单元测试扩展封装
|-- cloud-base-gateway -- Spring Cloud Gateway网关 [9999]
|-- cloud-base-visual --图形化组件
|   |-- cloud-base-codegen -- 图形化代码生成 [5002]
|   |-- cloud-base-monitor -- 服务监控 [5001]
|   |-- cloud-base-sentinel-dashboard -- 流量高可用 [5003]
|   `-- cloud-base-xxl-job-admin -- 分布式定时任务管理台 [5004]
|-- compose -- docker-compose应用部署配置文件
|-- db -- 数据库脚本文件
|-- docs -- 项目相关文档
`-- properties -- 服务配置文件备份
```

## 快速开始

### 本地开发 运行

开发环境安装、服务端代码运行、前端代码运行等。
[部署文档 wiki.pig4cloud.com](https://www.yuque.com/pig4cloud/pig/vsdox9)

_**PS: 请务必**完全按照**文档部署运行章节 进行操作，减少踩坑弯路！！**_

### Docker 运行

```shell script
# 下载并运行服务端代码
git clone https://github.com/wenming5112/spring-cloud-base.git

cd spring-cloud-base && mvn clean install && docker-compose up -d

# 下载并运行前端UI
git clone https://github.com/wenming5112/cloud-base-ui.git

cd cloud-base-ui && npm install -g cnpm --registry=https://registry.npm.taobao.org

npm run build:docker && docker-compose up -d
```

### 快速构架微服务

```shell script
# 在空文件夹执行以下命令，注意 windows 下  \ 修改成 ^
mvn archetype:generate \
       -DgroupId=com.pig4cloud \
       -DartifactId=demo \
       -Dversion=1.0.0-SNAPSHOT \
       -Dpackage=com.cloud.base.demo \
       -DarchetypeGroupId=com.pig4cloud.archetype \
       -DarchetypeArtifactId=pig-gen \
       -DarchetypeVersion=2.10.1 \
       -DarchetypeCatalog=local
```

### 运行时环境变量配置
```text

mysql_host=192.168.0.33;mysql_port=3306;nacos_host=192.168.0.33:8848;redis_url=123456@192.168.0.33:6379;-Ddruid.mysql.usePingMethod=false
```

### 问题记录
| 编号 |  问题 |  解决方案  |
|------|-------| ---------- |
| 1 | druid最新版本 警告 discard long time none received connection. 问题。| https://blog.csdn.net/zqlwcx/article/details/115732188
| 2 | Swagger Warn :Unable to interpret the implicit parameter configuration with dataType: ... dataTypeClass: class。| http://www.itwetouch.com/articles/2021/08/24/1629807101718.html
| 3 | IDEA中的.iml文件是项目标识文件，缺少了这个文件，IDEA就无法识别项目。 使用命令重新生成iml文件 | `mvn idea:module` |
| 4 | Auth模块内部登录页面无法直接走网关（内部静态资源通过网关之后无法访问，重定向之后没有auth前缀。），但可以直接访问Auth模块的登录页面。 | 暂无 |
| 5 | Auth模块Swagger文档访问问题,需要单点登录之后可访问。（User must be authenticated with Spring Security before authorization can be completed.）  | 在auth模块使用WebSecurityConfigurer 使用注解@Order(2)；ResourceServerConfiguration 和 SecurityConfiguration上配置的顺序,  SecurityConfiguration一定要在ResourceServerConfiguration 之前，因为spring实现安全是通过添加过滤器(Filter)来实现的，基本的安全过滤应该在oauth过滤之前, 所以在SecurityConfiguration设置@Order(2), 在ResourceServerConfiguration上设置@Order(6) |


### 问题修复或配置优化

- [x] 1、Auth模块Swagger文档访问问题,注入顺序问题 在auth模块使用WebSecurityConfigurer 使用注解@Order(2)
- [x] 2、采用druid数据源
- [ ] 3、采用seata分布式事务管理，暂不采用，seata1.4.2存在bug，待官方修复https://github.com/seata/seata/issues/1030
- [x] 4、单点登录问题
- [x] 5、修复druid 日志警告问题(discard long time none received connection ...)
- [ ] 6、监控程序配置
- [x] 7、优化logback-spring.xml配置文件
- [x] 8、系统日志存储新增存储到数据库。（前期存数据库，后期存储到ELK。）
- [x] 9、基于seata的分布式事务测试
- [x] 10、Auth模块使用seata时的异常Cannot build client services (maybe use inMemory() or jdbc()). （https://github.com/seata/seata/issues/1030）。暂不使用Seata作为分布式事务处理
- [x] 11、使用tlx事务管理

### 完成的功能

- [x] 短信验证码
- [x] 邮件验证码
- [x] 短信验证码模式登录
- [x] 密码模式登录
- [x] 图形验证码（有配置开关）
- [x] feign接口调用失败时的异常信息返回
- [x] sso 单点登录配置以及案例代码
- [x] oss 对象存储服务(阿里云)
- [x] tlxcn 分布式事务管理


### IM:
- https://gitee.com/farsunset/cim
